
interface CachedValue {
  value: string;
  expiracy: number;
}

class Cache {
  public timer: NodeJS.Timeout;
  private hashMap: Map<string, CachedValue> = new Map<string, CachedValue>();
  /**
   *
   * @param ttl Time To Live for Cached Values
   */
  constructor(private ttl: number, private max: number = 0) {
    // sets the interval of clearing to the half of the given ttl
    this.timer = setInterval(() => {
      for (let [key, value] of this.hashMap) {
        if (value.expiracy > Date.now()) this.hashMap.delete(key);
      }
    }, this.ttl / 2);
  }

  /**
   *
   * @param key sets the key value in the hashmap, updating the expiry time and value in the process
   * @param value
   */
  set(key: string, value: string) {
    if (this.max !== 0 && this.hashMap.size < this.max) this.hashMap.set(key, { value, expiracy: Date.now() + this.ttl });
  }
  /**
   *
   * @param key Looks into the cache and grabs key if it has it. Otherwise returns null
   */
  get(key: string): string | null {
    if (this.hashMap.has(key)) {
      const cachedItem = this.hashMap.get(key);
      return cachedItem && cachedItem.expiracy > Date.now() ? cachedItem.value : null;
    }
    return null;
  }
  // Rough proxy
  has(key: string): boolean {
    return this.hashMap.has(key);
  }
}

export default Cache;