"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cache {
    /**
     *
     * @param ttl Time To Live for Cached Values
     */
    constructor(ttl, max = 0) {
        this.ttl = ttl;
        this.max = max;
        this.hashMap = new Map();
        // sets the interval of clearing to the half of the given ttl
        this.timer = setInterval(() => {
            for (let [key, value] of this.hashMap) {
                if (value.expiracy > Date.now())
                    this.hashMap.delete(key);
            }
        }, this.ttl / 2);
    }
    /**
     *
     * @param key sets the key value in the hashmap, updating the expiry time and value in the process
     * @param value
     */
    set(key, value) {
        if (this.max !== 0 && this.hashMap.size < this.max)
            this.hashMap.set(key, { value, expiracy: Date.now() + this.ttl });
    }
    /**
     *
     * @param key Looks into the cache and grabs key if it has it. Otherwise returns null
     */
    get(key) {
        if (this.hashMap.has(key)) {
            const cachedItem = this.hashMap.get(key);
            return cachedItem && cachedItem.expiracy > Date.now() ? cachedItem.value : null;
        }
        return null;
    }
    // Rough proxy
    has(key) {
        return this.hashMap.has(key);
    }
}
exports.default = Cache;
