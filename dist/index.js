#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const redis_1 = require("redis");
const yargs_1 = require("yargs");
// Item is named RedisCache as there is already a Cache object.
const cache_1 = __importDefault(require("./cache"));
// Build out our options
const { argv } = yargs_1.options({
    port: {
        alias: 'p',
        description: 'The port to run the proxy server on',
        default: 3000
    },
    host: {
        alias: 'h',
        description: 'The address for the Redis host. This will have "redis://" prepended to the value.',
        default: 'localhost:6379'
    },
    ttl: {
        alias: 't',
        description: 'The time to live (in milliseconds) for each value stored in the cache. Defaults to 1 minute',
        default: 60 * 1000
    },
    'max-keys': {
        description: 'the maximum number of keys stored in the cache. 0 means unlimited',
        default: 0
    }
});
const cache = new cache_1.default(argv.ttl, argv['max-keys']);
const client = redis_1.createClient({
    url: 'redis://' + argv.host,
});
process.on('beforeExit', () => {
    if (client.connected)
        client.quit();
    clearInterval(cache.timer);
});
client.on('error', (err) => {
    console.error(err);
    process.exit();
});
const server = http_1.createServer((req, res) => {
    var _a, _b, _c;
    const { url } = req;
    const matches = (_a = url) === null || _a === void 0 ? void 0 : _a.match(/\/cache\/(?<key>\w+(?:\-\w+){0,})\/?/);
    // Check if we are using the correct route.
    if ((_c = (_b = matches) === null || _b === void 0 ? void 0 : _b.groups) === null || _c === void 0 ? void 0 : _c.key) {
        // get the key from the matches groups.
        const { key } = matches.groups;
        // GET the key value
        if (req.method === 'GET') {
            if (cache.has(key))
                return res.statusCode = 205, res.end(cache.get(key)), void 0;
            else {
                return client.get(key, (err, value) => {
                    if (err)
                        return res.statusCode = 500, res.end('An Error has ocurred'), console.log(err);
                    // if we do not have a value, then there is no content for the response
                    if (!value)
                        res.statusCode = 404;
                    // If we do have content, we need to update our proxy stash
                    else
                        cache.set(key, value);
                    res.end(value);
                });
            }
        }
        // Are we currently handling a PUT request? Guess we should PUT something new into the server
        else if (req.method === 'PUT') {
            // Gotta deal with the scope of variables
            let data = '';
            req.on('data', s => {
                // S is a buffer type by default -- not very human readable.
                data += s.toString();
            });
            req.on('end', () => {
                // try/catch because if body is malformed, this will throw an error when trying to parse data
                try {
                    // If this goes, then we should be able to look up what we need
                    const body = JSON.parse(data);
                    // Set the key, adding on the millisecond expiration
                    client.set(key, body.value, "PX", argv.ttl, (err, status) => {
                        // If we have an error, show it so that it would show up in our server logs, set the status code, and end the response
                        if (err)
                            return console.error(err), res.statusCode = 500, res.end('');
                        // Otherwise, set the key in our internal cache
                        cache.set(key, body.value);
                        // add the status code indicating we created a new thing internally
                        res.statusCode = 201;
                        // and end the response
                        res.end(status);
                    });
                }
                catch (_a) {
                    // If we are here, something very bad happened. Client error code, with a bit of warning
                    res.statusCode = 400;
                    res.end('Server could not parse data &ndash; Please check that it is valid before trying again');
                }
            });
        }
    }
    else {
        // We're a simple proxy, we don't need to deal with other requests
        res.statusCode = 404;
        res.end('');
    }
});
// Listen on the given port
server.listen(argv.port, () => {
    console.log(`listening on ${argv.port}`);
});
